terraform {
  backend "s3" {
    region = "ap-south-1"
    bucket = "ali-terraform-bucket" 
    key    = "fattehali/terraform.tfstate"
  }    
}